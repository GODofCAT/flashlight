package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    int red, green, blue;
    Boolean isRandom;

    SeekBar seekBarRed, seekBarGreen, seekBarBlue;
    LinearLayout linearLayoutMy;
    Button buttonRandom,buttonParty,buttonHfslfistm,buttonStop;
    Random random;

    Timer timer;
    TimerTask timerTask;
    boolean isStop = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutMy = findViewById(R.id.linearLayoutMy);

        red = green = blue = 255;

        random = new Random();

        isRandom = false;

        seekBarRed = findViewById(R.id.seekBarRed);
        seekBarGreen = findViewById(R.id.seekBarGreen);
        seekBarBlue = findViewById(R.id.seekBarBlue);

        seekBarRed.setProgress(red);
        seekBarGreen.setProgress(green);
        seekBarBlue.setProgress(blue);

        linearLayoutMy.setBackgroundColor(Color.rgb(red,green,blue));

        seekBarRed.setOnSeekBarChangeListener(seekBarOnChange);
        seekBarGreen.setOnSeekBarChangeListener(seekBarOnChange);
        seekBarBlue.setOnSeekBarChangeListener(seekBarOnChange);

        buttonRandom = findViewById(R.id.buttonRandom);
        buttonParty = findViewById(R.id.buttonParty);
        buttonHfslfistm = findViewById(R.id.buttonHelp);
        buttonStop = findViewById(R.id.buttonStop);

        buttonRandom.setOnClickListener(buttonRandomOnClick);
        buttonParty.setOnClickListener(buttonPartyTimeOnClick);
        buttonHfslfistm.setOnClickListener(buttonHfslfistmTimeOnClick);
        buttonStop.setOnClickListener(buttonStopTimeOnClick);
    }


    SeekBar.OnSeekBarChangeListener seekBarOnChange = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            if(isRandom==false)
            {
                red = seekBarRed.getProgress();
                green = seekBarGreen.getProgress();
                blue = seekBarBlue.getProgress();

                linearLayoutMy.setBackgroundColor(Color.rgb(red, green, blue));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    View.OnClickListener buttonRandomOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            randomColor();
        }
    };

    public void randomColor(){
        isRandom = true;

        red = random.nextInt(255);
        green = random.nextInt(255);
        blue = random.nextInt(255);

        seekBarRed.setProgress(red);
        seekBarGreen.setProgress(green);
        seekBarBlue.setProgress(blue);

        linearLayoutMy.setBackgroundColor(Color.rgb(red,green,blue));

        isRandom = false;
    }


    View.OnClickListener buttonPartyTimeOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!isStop)return;

            isStop=false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    randomColor();
                }
            }, 3000,1000);

        }
    };

    View.OnClickListener buttonHfslfistmTimeOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!isStop)return;

            isStop=false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    randomColor();
                }
            }, 100,100);


        }
    };

    View.OnClickListener buttonStopTimeOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isStop = true;
            timer.cancel();
        }
    };

}
